﻿using System;
using System.Collections.Generic;

namespace TuringPC
{
	public class TuringMachine
	{
		private CurrentState cur_state;
		private NextState next_state;
		public TuringMachine()
		{

		}
		public TuringMachine(CurrentState cur_state, NextState next_state)
		{
			this.cur_state = cur_state;
			this.next_state = next_state;
		}

		public CurrentState Cur_state { get => cur_state; set => cur_state = value; }
		public NextState Next_state { get => next_state; set => next_state = value; }
	}

	public class CurrentState
	{
		private string curr_state, read_sym;
		public CurrentState()
		{

		}

		public CurrentState(string curr_state, string read_sym)
		{
			this.curr_state = curr_state;
			this.read_sym = read_sym;
		}

		public string Curr_state { get => curr_state; set => curr_state = value; }
		public string Read_sym { get => read_sym; set => read_sym = value; }
	}

	public class NextState
	{
		private string new_state, write_sym, move;
		public NextState()
		{

		}

		public NextState(string new_state, string write_sym, string move)
		{
			this.new_state = new_state;
			this.write_sym = write_sym;
			this.move = move;
		}

		public string New_state { get => new_state; set => new_state = value; }
		public string Write_sym { get => write_sym; set => write_sym = value; }
		public string Move { get => move; set => move = value; }
	}
}