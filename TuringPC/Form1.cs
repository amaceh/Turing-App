﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TuringPC
{
	public partial class MainForm : Form
	{
		private List<TuringMachine> INSTRUKSI;
		private string INITIAL_STATE;
		private string FINISH_STATE;
		private string CURRENT_STATE;

		public MainForm()
		{
			InitializeComponent();
			state_label.Text = "not started";
		}

		private void InputPita()
		{
			PitaCleaner();
			string input = input_txt.Text;
			int i = 2;
			foreach (char inp in input)
			{
				PitaWriter(i, inp+"");
				i += 1;
			}
		}


		private void PitaWriter(int index, string value)
		{
			//akses pita, ouch index nya mulai dari satu, malas edit hahaha
			string name = "pita_"+index;
			this.Controls[name].Text = value;
		}

		private void PitaCleaner()
		{
			for(int i = 2; i<=21; i+=1)
			{
				string name = "pita_" + i;
				this.Controls[name].Text = "";
			}
		}


		//control method
		private void Input_Click(object sender, EventArgs e)
		{
			InputPita();
		}

		private void Copy_bin_bt_Click(object sender, EventArgs e)
		{
			/*
			 * Source for intruction...
			 * //Author: Gabriel Alvarez
				// Simulator: https://turingmachinesimulator.com
				// Initial state: qinit
				// Accepting state: qfin
				// Whole alphabet: 0,1,o,i

				// Comments: We duplicate the size of
				// a binary string by concatenating
				// the string to its mirror image.
				// i/o will be used as "markers" for
				// the squares with 1's and 0's
				// (respectively).

				name: Duplicate binary string
				init: qinit
				accept: qfin

				qinit,0
				qinit,0,>

				qinit,1
				qinit,1,>

				qinit,o
				qinit,0,>

				qinit,i
				qinit,1,>

				qinit,_
				copying_from_right_to_left,_,<

				//We place a marker (o/i) over the
				//first number found. We ignore the
				//squares already marked
				copying_from_right_to_left,0
				copying_0_to_the_right,o,>

				copying_from_right_to_left,1
				copying_1_to_the_right,i,>

				copying_from_right_to_left,o
				copying_from_right_to_left,o,<

				copying_from_right_to_left,i
				copying_from_right_to_left,i,<

				//If we find a blank square -->
				//we copy our value (marked) and
				//come back
				copying_0_to_the_right,_
				copying_from_right_to_left,o,<

				copying_1_to_the_right,_
				copying_from_right_to_left,i,<

				//If we find a non-blank square -->
				//we ignore it and keep moving right
				copying_0_to_the_right,0
				copying_0_to_the_right,0,>

				copying_0_to_the_right,1
				copying_0_to_the_right,1,>

				copying_0_to_the_right,o
				copying_0_to_the_right,o,>

				copying_0_to_the_right,i
				copying_0_to_the_right,i,>

				copying_1_to_the_right,0
				copying_1_to_the_right,0,>

				copying_1_to_the_right,1
				copying_1_to_the_right,1,>

				copying_1_to_the_right,o
				copying_1_to_the_right,o,>

				copying_1_to_the_right,i
				copying_1_to_the_right,i,>

				//When we finished copying the whole
				//string --> it's time to remove the
				//markers
				copying_from_right_to_left,_
				removing_the_markers,_,>

				removing_the_markers,o
				removing_the_markers,0,>

				removing_the_markers,i
				removing_the_markers,1,>

				removing_the_markers,0
				removing_the_markers,0,<

				removing_the_markers,1
				removing_the_markers,1,<

				removing_the_markers,_
				qfin,_,>
			 */
			INITIAL_STATE = "qinit";
			FINISH_STATE = "qfin";
			INSTRUKSI = new List<TuringMachine>();


			nama_label.Text = "Salin Biner";
			string resource_data = Properties.Resources.Instruksi_copy_biner;
			string[] words = resource_data.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
			string[] cur_st = null, nx_st; //null for shut up linter
			int i = 1;

			foreach (string lines in words)
			{
				if (i % 2 == 1)
				{
					cur_st = lines.Split(',');
				}
				else if (i % 2 == 0)
				{
					nx_st = lines.Split(',');
					INSTRUKSI.Add(new TuringMachine(new CurrentState(cur_st[0], cur_st[1]),
						new NextState(nx_st[0], nx_st[1], nx_st[2])));
				}
				i += 1;
			}
		}
	}
}
