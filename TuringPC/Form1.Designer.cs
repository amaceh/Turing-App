﻿namespace TuringPC
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pita_1 = new System.Windows.Forms.TextBox();
			this.pita_2 = new System.Windows.Forms.TextBox();
			this.pita_4 = new System.Windows.Forms.TextBox();
			this.pita_3 = new System.Windows.Forms.TextBox();
			this.pita_6 = new System.Windows.Forms.TextBox();
			this.pita_5 = new System.Windows.Forms.TextBox();
			this.pita_8 = new System.Windows.Forms.TextBox();
			this.pita_7 = new System.Windows.Forms.TextBox();
			this.pita_10 = new System.Windows.Forms.TextBox();
			this.pita_9 = new System.Windows.Forms.TextBox();
			this.pita_12 = new System.Windows.Forms.TextBox();
			this.pita_11 = new System.Windows.Forms.TextBox();
			this.pita_14 = new System.Windows.Forms.TextBox();
			this.pita_13 = new System.Windows.Forms.TextBox();
			this.pita_16 = new System.Windows.Forms.TextBox();
			this.pita_15 = new System.Windows.Forms.TextBox();
			this.pita_18 = new System.Windows.Forms.TextBox();
			this.pita_17 = new System.Windows.Forms.TextBox();
			this.pita_20 = new System.Windows.Forms.TextBox();
			this.pita_19 = new System.Windows.Forms.TextBox();
			this.pita_22 = new System.Windows.Forms.TextBox();
			this.pita_21 = new System.Windows.Forms.TextBox();
			this.input_txt = new System.Windows.Forms.TextBox();
			this.input_bt = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.start_bt = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.state_label = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.copy_bin_bt = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.nama_label = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pita_1
			// 
			this.pita_1.BackColor = System.Drawing.Color.White;
			this.pita_1.Enabled = false;
			this.pita_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_1.Location = new System.Drawing.Point(12, 65);
			this.pita_1.Name = "pita_1";
			this.pita_1.Size = new System.Drawing.Size(34, 49);
			this.pita_1.TabIndex = 0;
			this.pita_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_2
			// 
			this.pita_2.BackColor = System.Drawing.Color.White;
			this.pita_2.Enabled = false;
			this.pita_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_2.Location = new System.Drawing.Point(52, 65);
			this.pita_2.Name = "pita_2";
			this.pita_2.Size = new System.Drawing.Size(34, 49);
			this.pita_2.TabIndex = 1;
			this.pita_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_4
			// 
			this.pita_4.BackColor = System.Drawing.Color.White;
			this.pita_4.Enabled = false;
			this.pita_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_4.Location = new System.Drawing.Point(132, 65);
			this.pita_4.Name = "pita_4";
			this.pita_4.Size = new System.Drawing.Size(34, 49);
			this.pita_4.TabIndex = 3;
			this.pita_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_3
			// 
			this.pita_3.BackColor = System.Drawing.Color.White;
			this.pita_3.Enabled = false;
			this.pita_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_3.Location = new System.Drawing.Point(92, 65);
			this.pita_3.Name = "pita_3";
			this.pita_3.Size = new System.Drawing.Size(34, 49);
			this.pita_3.TabIndex = 2;
			this.pita_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_6
			// 
			this.pita_6.BackColor = System.Drawing.Color.White;
			this.pita_6.Enabled = false;
			this.pita_6.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_6.Location = new System.Drawing.Point(212, 65);
			this.pita_6.Name = "pita_6";
			this.pita_6.Size = new System.Drawing.Size(34, 49);
			this.pita_6.TabIndex = 5;
			this.pita_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_5
			// 
			this.pita_5.BackColor = System.Drawing.Color.White;
			this.pita_5.Enabled = false;
			this.pita_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_5.Location = new System.Drawing.Point(172, 65);
			this.pita_5.Name = "pita_5";
			this.pita_5.Size = new System.Drawing.Size(34, 49);
			this.pita_5.TabIndex = 4;
			this.pita_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_8
			// 
			this.pita_8.BackColor = System.Drawing.Color.White;
			this.pita_8.Enabled = false;
			this.pita_8.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_8.Location = new System.Drawing.Point(292, 65);
			this.pita_8.Name = "pita_8";
			this.pita_8.Size = new System.Drawing.Size(34, 49);
			this.pita_8.TabIndex = 7;
			this.pita_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_7
			// 
			this.pita_7.BackColor = System.Drawing.Color.White;
			this.pita_7.Enabled = false;
			this.pita_7.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_7.Location = new System.Drawing.Point(252, 65);
			this.pita_7.Name = "pita_7";
			this.pita_7.Size = new System.Drawing.Size(34, 49);
			this.pita_7.TabIndex = 6;
			this.pita_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_10
			// 
			this.pita_10.BackColor = System.Drawing.Color.White;
			this.pita_10.Enabled = false;
			this.pita_10.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_10.Location = new System.Drawing.Point(372, 65);
			this.pita_10.Name = "pita_10";
			this.pita_10.Size = new System.Drawing.Size(34, 49);
			this.pita_10.TabIndex = 9;
			this.pita_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_9
			// 
			this.pita_9.BackColor = System.Drawing.Color.White;
			this.pita_9.Enabled = false;
			this.pita_9.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_9.Location = new System.Drawing.Point(332, 65);
			this.pita_9.Name = "pita_9";
			this.pita_9.Size = new System.Drawing.Size(34, 49);
			this.pita_9.TabIndex = 8;
			this.pita_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_12
			// 
			this.pita_12.BackColor = System.Drawing.Color.White;
			this.pita_12.Enabled = false;
			this.pita_12.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_12.Location = new System.Drawing.Point(452, 65);
			this.pita_12.Name = "pita_12";
			this.pita_12.Size = new System.Drawing.Size(34, 49);
			this.pita_12.TabIndex = 11;
			this.pita_12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_11
			// 
			this.pita_11.BackColor = System.Drawing.Color.White;
			this.pita_11.Enabled = false;
			this.pita_11.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_11.Location = new System.Drawing.Point(412, 65);
			this.pita_11.Name = "pita_11";
			this.pita_11.Size = new System.Drawing.Size(34, 49);
			this.pita_11.TabIndex = 10;
			this.pita_11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_14
			// 
			this.pita_14.BackColor = System.Drawing.Color.White;
			this.pita_14.Enabled = false;
			this.pita_14.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_14.Location = new System.Drawing.Point(532, 65);
			this.pita_14.Name = "pita_14";
			this.pita_14.Size = new System.Drawing.Size(34, 49);
			this.pita_14.TabIndex = 13;
			this.pita_14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_13
			// 
			this.pita_13.BackColor = System.Drawing.Color.White;
			this.pita_13.Enabled = false;
			this.pita_13.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_13.Location = new System.Drawing.Point(492, 65);
			this.pita_13.Name = "pita_13";
			this.pita_13.Size = new System.Drawing.Size(34, 49);
			this.pita_13.TabIndex = 12;
			this.pita_13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_16
			// 
			this.pita_16.BackColor = System.Drawing.Color.White;
			this.pita_16.Enabled = false;
			this.pita_16.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_16.Location = new System.Drawing.Point(612, 65);
			this.pita_16.Name = "pita_16";
			this.pita_16.Size = new System.Drawing.Size(34, 49);
			this.pita_16.TabIndex = 15;
			this.pita_16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_15
			// 
			this.pita_15.BackColor = System.Drawing.Color.White;
			this.pita_15.Enabled = false;
			this.pita_15.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_15.Location = new System.Drawing.Point(572, 65);
			this.pita_15.Name = "pita_15";
			this.pita_15.Size = new System.Drawing.Size(34, 49);
			this.pita_15.TabIndex = 14;
			this.pita_15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_18
			// 
			this.pita_18.BackColor = System.Drawing.Color.White;
			this.pita_18.Enabled = false;
			this.pita_18.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_18.Location = new System.Drawing.Point(692, 65);
			this.pita_18.Name = "pita_18";
			this.pita_18.Size = new System.Drawing.Size(34, 49);
			this.pita_18.TabIndex = 17;
			this.pita_18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_17
			// 
			this.pita_17.BackColor = System.Drawing.Color.White;
			this.pita_17.Enabled = false;
			this.pita_17.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_17.Location = new System.Drawing.Point(652, 65);
			this.pita_17.Name = "pita_17";
			this.pita_17.Size = new System.Drawing.Size(34, 49);
			this.pita_17.TabIndex = 16;
			this.pita_17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_20
			// 
			this.pita_20.BackColor = System.Drawing.Color.White;
			this.pita_20.Enabled = false;
			this.pita_20.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_20.Location = new System.Drawing.Point(772, 65);
			this.pita_20.Name = "pita_20";
			this.pita_20.Size = new System.Drawing.Size(34, 49);
			this.pita_20.TabIndex = 19;
			this.pita_20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_19
			// 
			this.pita_19.BackColor = System.Drawing.Color.White;
			this.pita_19.Enabled = false;
			this.pita_19.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_19.Location = new System.Drawing.Point(732, 65);
			this.pita_19.Name = "pita_19";
			this.pita_19.Size = new System.Drawing.Size(34, 49);
			this.pita_19.TabIndex = 18;
			this.pita_19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_22
			// 
			this.pita_22.BackColor = System.Drawing.Color.White;
			this.pita_22.Enabled = false;
			this.pita_22.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_22.Location = new System.Drawing.Point(852, 65);
			this.pita_22.Name = "pita_22";
			this.pita_22.Size = new System.Drawing.Size(34, 49);
			this.pita_22.TabIndex = 21;
			this.pita_22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// pita_21
			// 
			this.pita_21.AcceptsReturn = true;
			this.pita_21.BackColor = System.Drawing.Color.White;
			this.pita_21.Enabled = false;
			this.pita_21.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pita_21.Location = new System.Drawing.Point(812, 65);
			this.pita_21.Name = "pita_21";
			this.pita_21.Size = new System.Drawing.Size(34, 49);
			this.pita_21.TabIndex = 20;
			this.pita_21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// input_txt
			// 
			this.input_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.input_txt.Location = new System.Drawing.Point(12, 133);
			this.input_txt.Name = "input_txt";
			this.input_txt.Size = new System.Drawing.Size(194, 29);
			this.input_txt.TabIndex = 22;
			// 
			// input_bt
			// 
			this.input_bt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.input_bt.Location = new System.Drawing.Point(12, 169);
			this.input_bt.Name = "input_bt";
			this.input_bt.Size = new System.Drawing.Size(194, 34);
			this.input_bt.TabIndex = 23;
			this.input_bt.Text = "INPUT";
			this.input_bt.UseVisualStyleBackColor = true;
			this.input_bt.Click += new System.EventHandler(this.Input_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(212, 133);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(182, 40);
			this.label1.TabIndex = 24;
			this.label1.Text = "Memori Maks 20 Char\r\nInput Hanya Biner";
			// 
			// start_bt
			// 
			this.start_bt.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.start_bt.Location = new System.Drawing.Point(692, 139);
			this.start_bt.Name = "start_bt";
			this.start_bt.Size = new System.Drawing.Size(194, 53);
			this.start_bt.TabIndex = 25;
			this.start_bt.Text = "START >>>";
			this.start_bt.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(11, 26);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(63, 20);
			this.label2.TabIndex = 26;
			this.label2.Text = "State :";
			// 
			// state_label
			// 
			this.state_label.AutoSize = true;
			this.state_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.state_label.Location = new System.Drawing.Point(80, 26);
			this.state_label.Name = "state_label";
			this.state_label.Size = new System.Drawing.Size(0, 20);
			this.state_label.TabIndex = 27;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.copy_bin_bt);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(15, 218);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(875, 79);
			this.groupBox1.TabIndex = 29;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Contoh";
			// 
			// copy_bin_bt
			// 
			this.copy_bin_bt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.copy_bin_bt.Location = new System.Drawing.Point(6, 28);
			this.copy_bin_bt.Name = "copy_bin_bt";
			this.copy_bin_bt.Size = new System.Drawing.Size(194, 34);
			this.copy_bin_bt.TabIndex = 30;
			this.copy_bin_bt.Text = "Salin Biner";
			this.copy_bin_bt.UseVisualStyleBackColor = true;
			this.copy_bin_bt.Click += new System.EventHandler(this.Copy_bin_bt_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(623, 26);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(63, 20);
			this.label6.TabIndex = 38;
			this.label6.Text = "State :";
			// 
			// nama_label
			// 
			this.nama_label.AutoSize = true;
			this.nama_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nama_label.Location = new System.Drawing.Point(692, 26);
			this.nama_label.Name = "nama_label";
			this.nama_label.Size = new System.Drawing.Size(0, 20);
			this.nama_label.TabIndex = 39;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(899, 310);
			this.Controls.Add(this.nama_label);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.state_label);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.start_bt);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.input_bt);
			this.Controls.Add(this.input_txt);
			this.Controls.Add(this.pita_22);
			this.Controls.Add(this.pita_21);
			this.Controls.Add(this.pita_20);
			this.Controls.Add(this.pita_19);
			this.Controls.Add(this.pita_18);
			this.Controls.Add(this.pita_17);
			this.Controls.Add(this.pita_16);
			this.Controls.Add(this.pita_15);
			this.Controls.Add(this.pita_14);
			this.Controls.Add(this.pita_13);
			this.Controls.Add(this.pita_12);
			this.Controls.Add(this.pita_11);
			this.Controls.Add(this.pita_10);
			this.Controls.Add(this.pita_9);
			this.Controls.Add(this.pita_8);
			this.Controls.Add(this.pita_7);
			this.Controls.Add(this.pita_6);
			this.Controls.Add(this.pita_5);
			this.Controls.Add(this.pita_4);
			this.Controls.Add(this.pita_3);
			this.Controls.Add(this.pita_2);
			this.Controls.Add(this.pita_1);
			this.Name = "MainForm";
			this.Text = "Turing Simulation";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox pita_1;
		private System.Windows.Forms.TextBox pita_2;
		private System.Windows.Forms.TextBox pita_4;
		private System.Windows.Forms.TextBox pita_3;
		private System.Windows.Forms.TextBox pita_6;
		private System.Windows.Forms.TextBox pita_5;
		private System.Windows.Forms.TextBox pita_8;
		private System.Windows.Forms.TextBox pita_7;
		private System.Windows.Forms.TextBox pita_10;
		private System.Windows.Forms.TextBox pita_9;
		private System.Windows.Forms.TextBox pita_12;
		private System.Windows.Forms.TextBox pita_11;
		private System.Windows.Forms.TextBox pita_14;
		private System.Windows.Forms.TextBox pita_13;
		private System.Windows.Forms.TextBox pita_16;
		private System.Windows.Forms.TextBox pita_15;
		private System.Windows.Forms.TextBox pita_18;
		private System.Windows.Forms.TextBox pita_17;
		private System.Windows.Forms.TextBox pita_20;
		private System.Windows.Forms.TextBox pita_19;
		private System.Windows.Forms.TextBox pita_22;
		private System.Windows.Forms.TextBox pita_21;
		private System.Windows.Forms.TextBox input_txt;
		private System.Windows.Forms.Button input_bt;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button start_bt;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label state_label;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button copy_bin_bt;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label nama_label;
	}
}

